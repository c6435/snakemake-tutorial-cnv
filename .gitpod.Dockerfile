FROM  condaforge/mambaforge
ENV SHELL /bin/bash 
ADD singularity.yml .
ADD snakemake.yml . 
ADD gdown.yml .
RUN mamba env create -n snakemake -f snakemake.yml
RUN mamba env create -n singularity -f singularity.yml
RUN mamba env create -n gdown -f gdown.yml
RUN apt-get update -q && \
    apt-get install -yq squashfs-tools
RUN apt-get update -q && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends tzdata